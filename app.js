const express = require("express"); // requerimos express
const morgan = require("morgan"); // requerimos morgan
const mongoose = require("mongoose"); // requerimos mongoose

const app = express(); // instanciamos express
const PUERTO = 3000; // número de puerto donde correrá nuestro servidor
const DIR_DB = "mongodb://localhost/proyecto-ma"; // dirección de nuestra base de datos mongodb

// conectamos a la base de datos.
// Aquí usamos una promesa
mongoose.connect(DIR_DB).then((db)=>{
    console.log("Hemos conectado con éxito a la base de datos.");
}).catch((err)=>{
    console.log("Oh no, parece que algo ha malido sal:");
    console.log(err);
});

// importamos las rutas que vamos a utilizar
const rutaCursos = require("./routes/cursos");

// configuramos el motor de vistas
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

// configuramos urlencoded para aceptar formularios
app.use(express.urlencoded({extended: true}));

//rutas a utilizar
app.use("/cursos", rutaCursos);

// configuramos morgan
app.use(morgan("dev"));

// corremos nuestro servidor
app.listen(PUERTO, ()=>{
    console.log("Express corriendo en el puerto... "+PUERTO);
});