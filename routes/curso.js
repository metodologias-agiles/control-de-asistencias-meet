const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");

const Periodo = require("../models/periodo");
const Curso = require("../models/curso");
const Clase = require("../models/clase");
const Asistencia = require("../models/asistencia");
const Alumno = require("../models/alumno");

const { render } = require("ejs");

//middleware
router.use((req, res, next)=>{
    next();
});

router.post("/:id", async (req, res, next)=>{
    res.redirect("/cursos");
});

module.exports = router;