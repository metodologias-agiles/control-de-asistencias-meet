// ruta de cursos

const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");

const Periodo = require("../models/periodo");
const Curso = require("../models/curso");
const Clase = require("../models/clase");
const Asistencia = require("../models/asistencia");
const Alumno = require("../models/alumno");
const Competencia = require("../models/competencia");
const { render } = require("ejs");

//middleware
router.use((req, res, next)=>{
    if(req.query._method=="POST"){
        req.method="POST";
        req.url = req.path;
    }
    else if(req.query._method=="GET"){
        req.method="GET";
        req.url = req.path;
    }
    next();
});

router.post("/", async (req, res, next)=>{
    //console.log(req.body);
    let dias = [];
    if(req.body.diaLunes){
        dias.push(req.body.diaLunes);
    }
    if(req.body.diaMartes){
        dias.push(req.body.diaMartes);
    }
    if(req.body.diaMiercoles){
        dias.push(req.body.diaMiercoles);
    }
    if(req.body.diaJueves){
        dias.push(req.body.diaJueves);
    }
    if(req.body.diaViernes){
        dias.push(req.body.diaViernes);
    }
    if(req.body.diaSabado){
        dias.push(req.body.diaSabado);
    }
    let curso = {
        nombre: req.body.nombre,
        horario: req.body.horario,
        dias: dias,
        periodo: req.body.periodo
    };
    let alta = new Curso(curso);
    await Curso.insertMany(alta);
    res.redirect("/cursos");
});

/* código viejo (solo referencia)
router.post("/:id", async (req, res, next)=>{
    let form = formidable({multiples: true});
    var fullPath = "";
    var dirPath = "";
    await form.parse(req, async (err, fields, files)=>{
        dirPath = "./files/"+req.params.id;
        fullPath = "./files/"+req.params.id+"/"+files.archivo.name;
        if(!err){
            try{
                if(!fs.existsSync(dirPath)){
                    fs.mkdirSync(dirPath);
                    console.log("Se ha creado la carpeta "+dirPath);
                }
                if(!fs.existsSync(fullPath)){
                    fs.copyFileSync(files.archivo.path, fullPath);
                    console.log("Se ha copiado "+files.archivo.name+" de "+files.archivo.path+" a "+fullPath);
                }
            }catch(e){
                console.log("Algo salió mal xddd")
                console.log(e);
            }
        }else{
            console.log("error: "+err);
        }
        
        let info = fs.readFileSync(fullPath, "UTF-8");
        let csv = papaparse.parse(info).data;
    
        res.render("mostrarDocumento",{csv});
    });

});
*/



router.get("/", async (req, res, next)=>{
    let periodos = await Periodo.find();
    let cursos = await Curso.find();
    res.render("cursos",{periodos, cursos});
});

// mostrar la vista de un curso usando su ID
// http://localhost:3000/cursos/5
router.get("/:id", async (req, res, next)=>{
    let curso = await Curso.find({_id: req.params.id});
    let unidadesCompetencia = await Competencia.find({idCurso: req.params.id});
    unidadesCompetencia = await unidadesCompetencia.sort(function(a, b){
        return a.indice-b.indice
    });
    res.render("curso", {curso, unidadesCompetencia});
});


router.post("/:id/nuevaUnidad", async (req, res, next)=>{
    let nuevaUnidadCompetencia = {
        idCurso: req.params.id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        indice: req.body.indice
    };
    let success = await Competencia.insertMany(nuevaUnidadCompetencia);
    res.redirect("/cursos/"+req.params.id);
});

router.post("/:id/registrarClase", async (req, res, next)=>{
    var competencia = undefined;
    var curso = undefined;
    let form = formidable({multiples: true});
    var fullPath = "";
    var dirPath = "";
    await form.parse(req, async (err, fields, files)=>{
        competencia = await Competencia.find({_id: fields.unidadCompetencia});
        curso = await Curso.find({_id: req.params.id});
        dirPath = "./files/"+req.params.id;
        fullPath = "./files/"+req.params.id+"/"+files.archivo.name;
        if(!err){
            try{
                if(!fs.existsSync(dirPath)){
                    fs.mkdirSync(dirPath);
                    console.log("Se ha creado la carpeta "+dirPath);
                }
                if(!fs.existsSync(fullPath)){
                    fs.copyFileSync(files.archivo.path, fullPath);
                    console.log("Se ha copiado "+files.archivo.name+" de "+files.archivo.path+" a "+fullPath);
                }
            }catch(e){
                console.log("Algo salió mal xddd")
                console.log(e);
            }
        }else{
            console.log("error: "+err);
        }
        
        let info = fs.readFileSync(fullPath, "UTF-8");
        let csv = papaparse.parse(info).data;
        let extraido = extraerInformacion(csv);
        let clase = await Clase.find({fecha: obtenerFecha(csv)});
        //console.log(clase);

        res.render("mostrarDocumento",{extraido, fullPath, competencia, curso, clase});
        
    });
});

// historia de usuario buscar clase por fecha
router.post("/:id/fecha", async (req, res, next)=>{
    let clase = await Clase.find({fecha: req.body.fecha});
    let curso = await Curso.find({_id: clase[0].idCurso});
    let competencia = await Competencia.find({_id: clase[0].idCompetencia});
    let asistencias = await Asistencia.find({idClase: clase[0]._id});
    let alumnos = [];
    for(let i = 0; i < asistencias.length; i++){
        let alumno = await Alumno.find({_id: asistencias[i].idAlumno});
        alumnos.push(alumno[0]);
    }
    console.log(alumnos);
    res.render("clase", {curso, competencia, clase, asistencias, alumnos});
});

router.post("/:id/altaClase", async (req, res, next)=>{
    let curso = await Curso.find({_id: req.params.id});
    let unidadCompetencia = await Competencia.find({_id: req.query.idUnidad});
    let dir = req.query.dir;
    //console.log("//////////////");
    //console.log(curso);
    //console.log(unidadCompetencia);
    //console.log(dir);
    let data = fs.readFileSync(dir, "UTF-8");
    let dataParseada = papaparse.parse(data).data;
    let info = extraerInformacion(dataParseada);

    for(let i = 0; i < info.length; i++){
        await Alumno.insertMany({
            nombre: info[i].nombre,
            idCurso: curso[0]._id,
            idUnidadCompetencia: unidadCompetencia[0]._id
        });
    }
    
    let alumnos = await Alumno.find();
    let alumnosID = [];
    for(let i = 0; i < alumnos.length; i++){
        alumnosID.push(alumnos[i]._id);
    }

    await Clase.insertMany({
        idCurso: curso[0]._id,
        idCompetencia: unidadCompetencia[0]._id,
        idAlumnos: alumnosID,
        fecha: obtenerFecha(dataParseada)
    });

    claseaux = await Clase.find({fecha: obtenerFecha(dataParseada)});
    //console.log("fecha: "+obtenerFecha(dataParseada));
    //console.log(claseaux);

    for(let i = 0; i < info.length; i++){
        if(info[i].asistencia != ""){
            await Asistencia.insertMany({
                idClase: claseaux[0]._id,
                idAlumno: alumnosID[i],
                asistencia: "1"
            });
        }else{
            await Asistencia.insertMany({
                idClase: claseaux[0]._id,
                idAlumno: alumnosID[i],
                asistencia: "0"
            });
        }
    }

    res.redirect("/cursos/"+req.params.id);
});

router.post("/:idCurso/:idCompetencia/:fecha/actualizarClase", async (req, res, next)=>{
    let form = formidable({multiples: true});
    await form.parse(req, async (err, fields, files)=>{
        let curso = await Curso.find({_id: req.params.idCurso});
        let competencia = await Competencia.find({_id: req.params.idCompetencia});
        let clase = await Clase.find({fecha: req.params.fecha});
        let asistencias = await Asistencia.find({idClase: clase[0]._id});
        
        for(let i = 0; i < asistencias.length; i++){
            console.log(fields["alumno"+(i+1)]);
            if(fields["alumno"+(i+1)]){
                await Asistencia.updateOne(
                    {
                        idClase: clase[0]._id,
                        idAlumno: fields["alumno"+(i+1)]
                    },
                    {
                        $set:{
                            asistencia: "1"
                        }
                    }
                );
                console.log("Se ha ingresado al if");
            }
            //*
            else{
                await Asistencia.updateOne(
                    {
                        idClase: clase[0]._id,
                        idAlumno: asistencias[i].idAlumno
                    },
                    {
                        asistencia: "0"
                    }
                );
                console.log("Se ha ingresado al else");
            }//*/
        }

    });

    res.redirect("/cursos/"+req.params.idCurso);

});

router.get("/:idCurso/lista", async (req, res, next)=>{
    let curso = await Curso.findOne({_id: req.params.idCurso});
    let competencias = await Competencia.find({idCurso: curso._id});
    let clases = await Clase.find({idCurso: curso._id});

    let alumnos = clases[0].idAlumnos;
    let info = {};
    for(let i = 0; i < alumnos.length; i++){
        
        let asistenciasTotales = 0;
        let faltasTotales = 0;
        let asistencias = await Asistencia.find({idAlumno: alumnos[i]});
        for(let j = 0; j < asistencias.length; j++){
            if(asistencias[j].asistencia == 1){
                asistenciasTotales = asistenciasTotales + 1;
            }else{
                faltasTotales = faltasTotales + 1;
            }
        }

        let unidades = [];
        for(let j = 0; j < competencias.length; j++){
            let clasesCompetencia = await Clase.find({idCurso: req.params.idCurso, idCompetencia: competencias[j]._id});
            let asistenciasCompetencia = 0;
            for(let k = 0; k < clasesCompetencia.length; k++){
                if(asistencias[k].asistencia == 1){
                    asistenciasCompetencia += 1;
                }
            }
            unidades.push(asistenciasCompetencia);
        }



        info[alumnos[i]] = {
            asistencias: asistenciasTotales,
            faltas: faltasTotales,
            asistenciasPorCompetencia: unidades
        };
       
    }
    let listaAlumnos = await Alumno.find({_id: alumnos});
    console.log(info);
    res.render("listaAsistencia",{info, curso, competencias, listaAlumnos});
});

router.get("/:idCurso/:idCompetencia", async (req, res, next)=>{
    let curso = await Curso.find({_id: req.params.idCurso});
    let competencia = await Competencia.find({_id: req.params.idCompetencia});
    let clases = await Clase.find({idCurso: req.params.idCurso, idCompetencia: req.params.idCompetencia});
    res.render("competencia", {curso, competencia, clases});
});

router.get("/:idCurso/:idCompetencia/:idClase", async (req, res, next)=>{
    let clase = await Clase.find({_id: req.params.idClase});
    let curso = await Curso.find({_id: req.params.idCurso});
    let competencia = await Competencia.find({_id: req.params.idCompetencia});
    let asistencias = await Asistencia.find({idClase: clase[0]._id});
    let alumnos = [];
    for(let i = 0; i < asistencias.length; i++){
        let alumno = await Alumno.find({_id: asistencias[i].idAlumno});
        alumnos.push(alumno[0]);
    }
    console.log(alumnos);
    res.render("clase", {curso, competencia, clase, asistencias, alumnos});
});


// elimina curso dado su ID
router.post("/eliminar/curso/c/:idCurso", async (req, res, next)=>{
    let id = req.params.idCurso;
    await Curso.deleteMany({_id: id});
    await Competencia.deleteMany({idCurso: id});
    await Clase.deleteMany({idCurso: id});
    await Alumno.deleteMany({idCurso: id});
    res.redirect("/cursos");
});

router.post("/eliminar/competencia/c/:idCompetencia", async (req, res, next)=>{
    let id = req.params.idCompetencia;
    await Competencia.deleteMany({_id: id});
    await Clase.deleteMany({idCurso: id});
    res.redirect("/cursos");
});

router.post("/actualizar/curso/c/:idCurso", async (req, res, next)=>{
    let id = req.params.idCurso;
    let curso = {
        nombre: req.body.nombre,
        horario: req.body.horario,
        dias: req.body.dias,
        periodo: req.body.periodo
    }
    await Curso.updateOne({_id: id}, curso);
    res.redirect("/cursos")
});

router.post("/actualizar/competencia/c/:idCompetencia", async (req, res, next)=>{
    let id = req.params.idCompetencia;
    let competencia = {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        indice: req.body.indice
    }
    await Competencia.updateOne({_id: id}, competencia);
    res.redirect("/cursos")
});

router.get("/actualizar/curso/ver/a/:idCurso", async (req, res, next)=>{
    let id = req.params.idCurso;
    let curso = await Curso.findOne({_id: id});
    let periodos = await Periodo.find();
    res.render("actualizarCurso", {curso, periodos});
});

router.get("/actualizar/competencia/ver/a/:idCompetencia", async (req, res, next)=>{
    let id = req.params.idCompetencia;
    let competencia = await Competencia.findOne({_id: id});
    console.log(competencia);
    res.render("actualizarCompetencia", {competencia});
});

module.exports = router;


function extraerInformacion(data){
    
    let nombres = [];
    for(let i = 3; ; i++){
        if(data[i][0]){
            nombres.push(data[i][0]);
        }else{
            break;
        }
    }

    let asistencias = [];
    for(let i = 3; i < nombres.length+3; i++){
        if(data[i][1]){
            asistencias.push(data[i][1]);
        }else{
            asistencias.push("");
        }
    }

    alumnos = [];
    for(let i = 0; i < nombres.length; i++){
        alumnos.push({
            "nombre": nombres[i],
            "asistencia": asistencias[i],
            "fecha": obtenerFecha(data)
        });
    }

    return alumnos;

}

function obtenerFecha(data){
    return data[0][0].substr(data[0][0].length-10, 10);
}