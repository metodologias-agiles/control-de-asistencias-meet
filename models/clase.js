const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const clase_scheme = new Schema({
    idCurso:String,
    idCompetencia:String,
    idAlumnos:Array,
    fecha:String
});

module.exports = mongoose.model("clases", clase_scheme);
