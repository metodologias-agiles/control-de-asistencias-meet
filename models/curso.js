const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const curso_scheme = new Schema({
    nombre:String,
    horario:String,
    dias:Array,
    periodo:String
});

module.exports = mongoose.model("cursos", curso_scheme);
