const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const periodo_scheme = new Schema({
    id:Number,
    nombre:String
});

module.exports = mongoose.model("periodos", periodo_scheme);
