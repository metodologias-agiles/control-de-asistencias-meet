const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const alumno_scheme = new Schema({
    nombre: String,
    idCurso: String,
    idUnidadCompetencia: String,
});

module.exports = mongoose.model("alumnos", alumno_scheme);
