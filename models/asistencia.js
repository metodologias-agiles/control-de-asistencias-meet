const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const asistencia_scheme = new Schema({
    idClase: String,
    idAlumno: String,
    asistencia: String
});

module.exports = mongoose.model("asistencias", asistencia_scheme);
