const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const competencia_scheme = new Schema({
    idCurso:String,
    nombre:String,
    descripcion:String,
    indice:Number
});

module.exports = mongoose.model("competencias", competencia_scheme);
